import placeHolderAPI from '../api/placeHolderAPI';
import { FETCH_USERS, FETCH_USER } from '../reducers/types';

export const fetchUsers = () => async (dispatch, getState) => {
  const response = await placeHolderAPI.get('/users');
  dispatch({ type: FETCH_USERS, payload: response.data });
}

export const fetchUser = id => async (dispatch, getState) => {
  const response = await placeHolderAPI.get(`/users/${id}`);
  dispatch({ type: FETCH_USER, payload: response.data });
}