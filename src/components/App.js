import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import UsersList from './users/UsersList';
import Navbar from './Navbar';
import UserDetails from './users/UserDetails';

const App = () => {
  return(
    <BrowserRouter>
      <div>
        <Navbar />
        <div className="container">
          <Switch>
              <Route path="/" exact component={UsersList}/>
              <Route path="/user/:id" exact component={UserDetails}/>
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  )
}
export default App;