import React, { Component } from 'react';
import CustomInput from './CustomInput';

class CustomForm extends Component {
  state = { fieldList : null }

  beautifyTitle(title){
    return title.charAt(0).toUpperCase() + title.substring(1)
  }

  componentWillMount(){
    this.generateListField();
  }

  generateListField(){
    let fieldList = []
    for(let field in this.props.dataSet){
      
      if(typeof this.props.dataSet[field] === 'object' || field === 'id'){
        continue;
      }

      const fieldSettings = {
        type: 'text',
        disabled: true,
        label: field,
        value: this.props.dataSet[field]
      }

      fieldList.push(fieldSettings)
    }
    this.setState({ fieldList })
  }

  renderFieldList(){
    return this.state.fieldList.map((field, index) => {
      return(
        <CustomInput
          key={index}
          isDisabled={field.disabled}
          type={field.type}
          label={this.beautifyTitle(field.label)}
          value={field.value}
        />
      )
    });
  }


  render(){
    return(
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{this.props.formTitle}</h5>
          {this.renderFieldList()}
        </div>
      </div>
    )
  }
};

export default CustomForm;