import React from 'react';

const CustomInput = ({ inputType, isDisabled = false , placeholder, label, value }) => {
  const type = inputType ? inputType : 'text';
  return(
    <div className="form-group">
      <label>{label}</label>
      <input 
        className="form-control" 
        type={type}
        disabled={isDisabled}
        value={value}
      />
    </div>
  )
}

export default CustomInput;