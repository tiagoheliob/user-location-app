import React from 'react';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'


const MapUserLocaction = ({ lat , lng , zoom, mapsize }) => {

  return(
    <Map center={{lat, lng}} zoom={zoom} className="map-user-location" style={{ height: mapsize }}>
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={{lat, lng}}>
        <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
      </Marker>
    </Map>
  )
}

export default MapUserLocaction;