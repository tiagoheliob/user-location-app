import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUser } from '../../actions'
import Loader from '../Loader';
import CustomForm from '../CustomForm';
import UserLocation from './UserLocation';

class UserDetails extends Component{

  componentDidMount(){
    const { id } = this.props.match.params;
    this.props.fetchUser(id);
    
  }

  render(){
    
    if(!this.props.user){
      return <Loader loadMessage="Fetching user data..."/>;
    }

    const userAddress = this.props.user.address;
    return(
      <div className="row">
        <div className="col-md-8">
          <CustomForm
            formTitle="User Information"
            dataSet={this.props.user}
          />
        </div>
        <div className="col-md-4">
          <UserLocation userAddress={userAddress} />
        </div>
      </div>
    );
  }
}

const MapStateToProps = (state, ownProps) => {
  return{
    user: state.users[ownProps.match.params.id]
  }
}

export default connect(MapStateToProps, { fetchUser })(UserDetails);