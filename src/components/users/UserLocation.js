import React from 'react';
import MapUserLocaction from './MapUserLocation';
import './UserLocation.css';

const UserLocation = ({ userAddress }) => {
  const {
    street,
    suite,
    city,
    zipcode,
    geo
  } = userAddress;
  return(
    <div className="card user-location-card">
      <h4>User Address</h4>
      <p>Street: {street}</p>
      <p>Suite: {suite}</p>
      <p>City: {city}</p>
      <p>Zipcode: {zipcode}</p>
      <MapUserLocaction 
        lat={geo.lat}
        lng={geo.lng}
        zoom={12}
        mapsize='300px'
      />
    </div>
  );
}

export default UserLocation;