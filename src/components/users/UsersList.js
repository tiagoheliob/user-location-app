import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchUsers } from '../../actions';
import './UsersList.css'
import Loader from '../Loader';

class UsersList extends Component{
  
  componentDidMount(){
    this.props.fetchUsers();
  }

  renderList(){
    
    
    if(!this.props.users.length){
      return <Loader loadMessage="Fetching user list..."/>;
    }

    return(
      this.props.users.map(({ name, id }) => {
        return (
          <li key={id} className="list-group-item">
            <Link to={`/user/${id}`}>{name}</Link>
          </li>
        )
      })
    )
  }

  render(){
    
    return(
      <ul className="list-group">
        {this.renderList()}
      </ul>
    )
  }
}

const mapStateToProps = ({ users }) => {
  return{
    users: Object.values(users)
  }
}

export default connect(mapStateToProps, { fetchUsers })(UsersList);