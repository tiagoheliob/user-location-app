import { combineReducers } from 'redux';

import {
  FETCH_USER,
  FETCH_USERS,
} from './types';

import { newMapper } from './utils';

const userReducer = (state = [], action) => {
  switch(action.type){
    case FETCH_USERS:
      return newMapper(action.payload, 'id');
    case FETCH_USER:
      return newMapper(action.payload, 'id');
    default:
      return state;
  }
}

export default combineReducers({
  users: userReducer
});