export const newMapper = (allData, key) => {

  if(!Array.isArray(allData)){
    return { [allData[key]] : allData }
  }
  const mapper = {};
  allData.forEach(data => {
    mapper[data[key]] = data;
  });
  
  return mapper;
}